function baseUrl() {
  return 'http://localhost:8089/'
}
let base = baseUrl()

let url = {
  addSurvey: base + 'survey/add',
  surveyQuery: base + 'survey/search',
  getAllSurvey: base + 'survey/getAllSurvey',
  getSurveyCondition: base + 'survey/getSurveysByCondition',
  deleteSurvey: base + 'survey/deleteSurvey',
  saveSurvey: base + 'surveyDetail/saveSurveyDetail', // 保存问卷
  savePubSurvey: base + 'surveyDetail/savePubSurveyDetail', // 保存并发布问卷
  pubSurvey: base + 'survey/changeStatus', // 发布问卷，仅保存后的问卷发布,
  startDesign: base + 'survey/startDesign', // 开始设计问卷
  saveSetting: base + 'survey/saveSetting', // 保存设置结果
  findSurveyDetail: base + 'survey/previewSurvey', // 预览要发布的问卷
}

export default url
