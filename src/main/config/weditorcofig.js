/**
 * wangEditor 参数配置
 */
module.exports = {
  uploadImgServer: 'http://localhost:8089/editorImage/upload',
  uploadImgMaxSize: 3 * 1024 * 1024,
  uploadFileName: 'file',
  uploadImgParams: {
    //自定义的上传参数
    token: '123abc',
  },
}
