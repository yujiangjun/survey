import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      children: [
        {
          path: '/',
          name: 'mysurvey',
          component: () => import('./views/mysurvey.vue'),
        },
        {
          path: '/design',
          name: 'design',
          component: () => import('./views/design.vue'),
        },
      ],
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */
        './views/About.vue'),
    },
    {
      path: '/preview',
      name: 'preview',
      component: () => import('./views/preview.vue'),
    },
    {
      path: '/phonepre',
      name: 'phonepre',
      component: () => import('./views/iphonepre.vue'),
    },
    {
      path: '/premobile',
      name: 'premobile',
      component: () => import('./views/premobile.vue'),
    },
  ],
})
